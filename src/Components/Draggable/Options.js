const properties = {
    addClasses: true,
    appendTo: 'parent',
    axis: false,
    cancel: 'input,textarea,button,select,option',
    classes: {},
    connectToSortable: false,
    containment: false,
    cursor: 'auto',
    cursorAt: false,
    delay: 0,
    disabled: false,
    distance: 1,
    grid: false,
    handle: false,
    helper: 'original',
    iframeFix: false,
    opacity: false,
    refreshPositions: false,
    revert: false,
    revertDuration: 500,
    scope: 'default',
    scroll: true,
    scrollSensitivity: 20,
    scrollSpeed: 20,
    snap: false,
    snapMode: 'both',
    snapTolerance: 20,
    stack: false,
    zIndex: false
}

const events = {
    onCreate: false,
    onDrag: false,
    onStart: false,
    onStop: false
}

export {
    properties,
    events
}
