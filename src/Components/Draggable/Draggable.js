import React, { useEffect, useState, createRef, Fragment } from 'react';
import jQuery from 'jquery';
import 'jquery-ui/ui/widgets/draggable';
import { properties, events } from './Options';

const Draggable = ({
    opts,
    children,
    onCreate,
    onStart,
    onDrag,
    onStop,
    element = 'div'
}) => {

    // setting the tag name for drag
    const Tag = `${element}`;

    // creatingthe elemnt reference
    const ref = createRef();

    // cloning the option to local state to handle internally
    const [options, setOptions] = useState(properties);

    // overridding the the default create event
    events.onCreate = onCreate || false;

    // override default start event
    events.onStart = onStart || false;

    // overriding the default drag events
    events.onDrag = onDrag || false;

    // overridding the default stop event
    events.onStop = onStop || false;

    // assign the props options to local state
    useEffect(() => {
        console.log("opts : ", opts);
        setOptions({...options, ...opts});
    }, [opts]);

    // initializing the jquery draggable
    useEffect(() => {
        const $node = jQuery(ref.current);

        $node.draggable({
            ...options,
            create: (e, ui) => {
                // calling the passed onCreate event if its exists
                events.onCreate ?
                    events.onCreate(ref.current, ui)
                    : false;
            },
            start:  (e, ui) => {
                // calling the passed onStart events if exists
                events.onStart ?
                    events.onStart(ref.current, ui)
                    : false;
            },
            drag: (e, ui) => {
                // calling the the passed onDrag function if exists
                events.onDrag ?
                    events.onDrag(ref.current, ui)
                    : false;
            },
            stop:  (e, ui) => {
                // calling the passsed onStop event if exists
                events.onStop ?
                    events.onStop(ref.current, ui)
                    : false;
            }
        });
    }, [options])

    return (
        <Tag ref={ref}>
            {children}
        </Tag>
    )

}

export { Draggable };
