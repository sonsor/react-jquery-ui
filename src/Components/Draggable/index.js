import [ Draggable ] from './Draggable';
import PropTypes from 'prop-types';

Draggable.propTypes = {

    /**
     * options object for the draggable ui
     * check https://api.jqueryui.com/draggable/
     */
    opts: PropTypes.object.isRequired,

    /**
     * children components fo react to render under draggable wrappper
     */
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired,

    /**
     * create event of jquery dragable
     * check https://api.jqueryui.com/draggable/
     */
    onCreate: PropTypes.func,

    /**
     * drag start even of jquery draggable
     * check https://api.jqueryui.com/draggable/
     */
    onStart: PropTypes.func,

    /**
     * dragging event event of jquery draggable
     * check https://api.jqueryui.com/draggable/
     */
    onDrag: PropTypes.func,

    /**
     * drag dtop event of jquery draggable
     * check https://api.jqueryui.com/draggable/
     */
    onStop: PropTypes.func,

    /**
     * Tag to use the wrap the draggable components
     * it should be string eiher html element tag or react componet elment
     */
    element: PropTypes.string.isRequired,
}

export { Draggable }