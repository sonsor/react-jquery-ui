import React from 'react';
import ReactDOM from "react-dom";
import { Draggable } from '../../index';


const App = () => {

    const opts = {
        helper: 'clone'
    };

    return (
        <Draggable opts={opts} element="div">
            <h1>Wasif</h1>
        </Draggable>
    )
}

ReactDOM.render(<App />, document.getElementById('root'));
//module.hot.accept();
